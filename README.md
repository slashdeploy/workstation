# Workstation

![](http://luminetrics.net/blog/wp-content/uploads/2013/08/TOR-001.jpg)

> One VM to rule them all, One VM to find them, One VM to bring them
> all, and in the darkness bind them.

Want to learn more? Check out the [blog post][blog]!

## Installation

First create a directory on your machine to hold all slashdeploy
repositories. The instructions assume `~/slashdeploy`, but you're welcome
to use any directory you want. This value must be set in the
`SLASHDEPLOY_PROJECT_PATH` environment variable. Put this or it's
equivalent in your shell config:

	export SLASHDEPLOY_PROJECT_PATH=~/slashdeploy

Next enter that directory and clone this repository

	$ cd "$SLASHDEPLOY_PROJECT_PATH"
	$ git clone git@gilab.com:slashdeploy/workstation.git --recursive
	$ cd workstation

Now it's time to configure your host system. Althought most of the
heavy lifting is done by the workstation virtual machine, your host
system still needs to support the virtual machine. This requires some
dependencies and setting other environment variables. There is a test
program that verifies your host system. Run:

	$ script/host-check

Repeat and follow the instructions until everything is configured
correctly.

	$ slashdeploy-workstation up
	$ slashdeploy-workstation provision

Last but not least it's time to clone all slashdeploy code to your
machine.

	$ script/clone

Now you should be ready to go! You should alias `slashdeploy-workstation`
to something shorter in your shell. This will save you a ton of
typing!

## Upgrading

Upgrades notices come from the SRE team. Upgrading happens with the following command:

	$ slashdeploy-workstation upgrade

`slashdeploy-workstation upgrade` command is a three step process which involves:

1. A `git pull` to get the latest code
1. `git submodule update` to update vendored dependnecies
1. `slashdeploy-workstation provision` to reconfigure the workstation

NOTE: Upgrades may require you to configure more environment variables.

## How it works

* We boot a vagrant-machine and set it up to have essential tools
* `$SLASHDEPLOY_PROJECT_PATH` is mounted at `/projects` inside the VM
* Aliases are used to easily run commands inside the VM under the
	relative path
* `$ slashdeploy-workstation run -- whatever` can be used to run whatever

## Project Shortcuts

These commands are intended to run against a particular project.

* `ci` - `make test-ci`
* `clean` - `make test-clean`
* `make` - `make`
* `shell` - Open terminal
* `test` - `make test`

## Global Commands

These commands may be invoked from anywhere since they are not project
specific.

* `aws`
* `docker`

## Testing

`make` coordinates the test process. The test process provisions a new
VM from scratch and ensure all the appropriate things went off
correctly.

	$ make clean test-ci

[blog]: http://blog.slashdeploy.com/2016/07/02/dev-environments/

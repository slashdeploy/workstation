PROJECT_PATH:=$(CURDIR)/tmp/projects
TEST_ENV:=\
	SLASHDEPLOY_WORKSTATION_NAME=slashdeploy-test \
	SLASHDEPLOY_PROJECT_PATH=$(PROJECT_PATH) \
	SLASHDEPLOY_AWS_ACCESS_KEY_ID=placeholder \
	SLASHDEPLOY_AWS_SECRET_ACCESS_KEY=placeholder \
	SLASHDEPLOY_AWS_DEFAULT_REGION=eu-west-1 \
	SLASHDEPLOY_INFRASTRUCTURE_VAULT_PASSWORD=placeholder \
	SLASHDEPLOY_KPI_VAULT_PASSWORD=placeholder

SD:=env $(TEST_ENV) $(CURDIR)/bin/sd

.PHONY: check
check:
	vagrant --version

$(PROJECT_PATH):
	mkdir -p $@

.PHONY: test-ci
test-ci: | $(PROJECT_PATH)
	mkdir -p $|/test-project
	$(SD) up
	$(SD) provision
	sh -c 'cd $|/test-project && $(SD) run true'
	$(SD) destroy -f

.PHONY: clean
clean: | $(PROJECT_PATH)
	-$(SD) destroy -f
	rm -rf $(PROJECT_PATH)

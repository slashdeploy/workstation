#!/usr/bin/env bash

set -euo pipefail

pushd /vagrant

sudo -EHu "${1}" ansible-playbook site.yml \
	--extra-vars "workstation_user=${1}"

#!/usr/bin/env bash

set -eou pipefail

apt-get update -y
apt-get install -y \
	libffi-dev \
	libssl-dev \
	libxml2-dev \
	libyaml-dev \
	libxslt1-dev \
	software-properties-common \
	python2.7 \
	python-pip \
	python-dev

apt-get autoremove -y

pip install "ansible==2.2.0.0"
pip install boto
pip install cryptography
pip install docker-py

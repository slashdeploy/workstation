# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"
  config.vm.box_check_update = false

  # Enable SSH forwarding (required for ansible deploys)
  config.ssh.forward_agent = true
  config.ssh.insert_key = true

  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM
    vb.memory = ENV.fetch("SLASHDEPLOY_WORKSTATION_MEMORY", 1024).to_i
  end

	if ENV.key? "WORKSTATION_PROJECT_PATH"
    config.vm.synced_folder ENV.fetch("WORKSTATION_PROJECT_PATH"), "/projects"
  end

  config.vm.provision "shell", path: "provisioners/install-ansible.sh"
  config.vm.provision("shell", {
    path: "provisioners/run-playbook.sh",
    args: [ "ubuntu" ]
  })

  # Blog Port
  config.vm.network "forwarded_port", guest: 4000, host: 7000

  # KubeProxy
  config.vm.network "forwarded_port", guest: 8001, host: 7001

  # TODO: Broken on vagrant 1.8.1, fixed scheduled for Vagrant 1.8.2
  # config.vm.provision "ansible_local" do |ansible|
  #   ansible.provisioning_path = "/vagrant"
  #   ansible.playbook = "site.yml"
  #   ansible.install = false
  #   ansible.verbose = true
  # end
end
